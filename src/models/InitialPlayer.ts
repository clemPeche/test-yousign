import { Card } from "./Card";

export interface InitialPlayer {
  cards: Card[];
  score: number;
  hasWon: boolean;
}
