type CardValues =
  | "2"
  | "3"
  | "4"
  | "5"
  | "6"
  | "7"
  | "8"
  | "9"
  | "10"
  | "JACK"
  | "QUEEN"
  | "KING"
  | "ACE";

export interface Card {
  image: string;
  value: CardValues;
  suit: string;
  code: string;
}
