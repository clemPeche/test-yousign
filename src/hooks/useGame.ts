import { useEffect, useState, useCallback } from "react";

import useService from "./useService";
import { initialPlayer } from "../constants/initialPlayer";
import { getScore } from "../helpers/getScore";

export default () => {
  const { getNewShuffledDeck, drawCard } = useService();

  const [deckId, setDeckId] = useState("");
  const [fetching, setFetching] = useState(false);
  const [bank, setBank] = useState(initialPlayer);
  const [player, setPlayer] = useState(initialPlayer);
  const [isBankPlaying, setIsBankPlaying] = useState(false);
  const [isGameFinished, setIsGameFinished] = useState(true);

  // Brings a brand new deck
  useEffect(() => {
    if (isGameFinished) {
      setFetching(true);
      try {
        getNewShuffledDeck().then(({ data }) => {
          setDeckId(data.deck_id);
          setFetching(false);
        });
      } catch (error) {
        console.error("something went wrong : ", error);
      }
    }
  }, [isGameFinished]);

  const clearState = useCallback(() => {
    setIsBankPlaying(false);
    setIsGameFinished(false);
    setBank({ ...initialPlayer });
    setPlayer({ ...initialPlayer });
  }, [isBankPlaying, isGameFinished, bank, player]);

  const initialDraw = async () => {
    setFetching(true);
    try {
      const {
        data: { cards },
      } = await drawCard(deckId, 4);

      const bankCards = cards.splice(0, 2);
      const bankScore = getScore(bankCards);
      setBank((bank) => ({ ...bank, cards: bankCards, score: bankScore }));

      const playerCards = cards.splice(0, 2);
      const playerScore = getScore(playerCards);
      setPlayer((player) => ({
        ...player,
        cards: playerCards,
        score: playerScore,
      }));
      setFetching(false);
    } catch (error) {
      console.error("something went wrong : ", error);
    }
  };

  const startGame = () => {
    clearState();
    initialDraw();
  };

  const hit = async () => {
    const {
      data: { cards },
    } = await drawCard(deckId, 1);

    if (!isBankPlaying) {
      const newPlayerCards = [...player.cards, cards[0]];
      const playerScore = getScore(newPlayerCards);
      setPlayer({ ...player, cards: newPlayerCards, score: playerScore });
    } else {
      const bankCards = [...bank.cards, cards[0]];
      const bankScore = getScore(bankCards);

      setBank({ ...player, cards: bankCards, score: bankScore });
    }
  };

  const stay = () => setIsBankPlaying(true);

  // Defines if the game should end
  useEffect(() => {
    if (
      bank.score > 21 ||
      player.score > 21 ||
      (isBankPlaying && bank.score >= player.score)
    ) {
      setIsBankPlaying(true);
      setIsGameFinished(true);
    } else if (isBankPlaying && !isGameFinished) {
      setTimeout(() => {
        hit();
      }, 1000);
    }
  }, [isBankPlaying, bank.score, player.score, isGameFinished]);

  // Set the Winner
  useEffect(() => {
    if (isGameFinished && isBankPlaying) {
      if (
        (bank.score >= player.score && bank.score <= 21) ||
        player.score > 21
      ) {
        setBank((bank) => ({ ...bank, hasWon: true }));
      } else {
        setPlayer((player) => ({ ...player, hasWon: true }));
      }
    }
  }, [isGameFinished, bank.score]);

  return {
    startGame,
    hit,
    stay,
    setIsBankPlaying,
    fetching,
    bank,
    player,
    isGameFinished,
    isBankPlaying,
  };
};
