import { createContext, useContext } from "react";

import api from "../services/Api";

const context = createContext(api);

export default () => useContext(context);
