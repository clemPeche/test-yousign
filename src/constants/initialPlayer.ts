import { InitialPlayer } from "../models/InitialPlayer";

export const initialPlayer: InitialPlayer = {
  cards: [],
  score: 0,
  hasWon: false,
};
