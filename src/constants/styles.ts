export const colors = {
  fountainBlue: "#63bfbf",
  juniper: "#728C8A",
  diserria: "#d99152",
  atronaut: "#295773",
  swirl: "#D3C8C0",
  white: "#ffffff",
};

export const screens = {
  medium: "900px",
};
