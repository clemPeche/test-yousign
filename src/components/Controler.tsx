import React, { FunctionComponent } from "react";

import { HitButton, StayButton, StarterButton } from "../styles/Buttons";
import { ControlerBanner, ControlerWrapper } from "../styles/Controler";
import { Label } from "../styles/Label";

interface Props {
  isGameFinished: boolean;
  bankHasWon: boolean;
  playerHasWon: boolean;
  startGame?(): void;
  hit?(): void;
  stay?(): void;
}

const Controler: FunctionComponent<Props> = ({
  isGameFinished,
  bankHasWon,
  playerHasWon,
  startGame,
  hit,
  stay,
}) => (
  <ControlerBanner>
    <ControlerWrapper hide={isGameFinished}>
      <HitButton onClick={hit}>Hit !</HitButton>
      <StayButton onClick={stay}>Stay</StayButton>
    </ControlerWrapper>
    <ControlerWrapper hide={!isGameFinished}>
      {bankHasWon && <Label>You lose</Label>}
      {playerHasWon && <Label>You win</Label>}
      <StarterButton onClick={startGame}>Start game</StarterButton>
    </ControlerWrapper>
  </ControlerBanner>
);
export default Controler;
