import React, { FunctionComponent } from "react";

import { Card as CardInterface } from "../models/Card";
import { CardWrapper, CardImage } from "../styles/Cards";

interface Props {
  card: CardInterface;
  hide: boolean;
  index: number;
}

const Card: FunctionComponent<Props> = ({ card, hide, index }) => (
  <CardWrapper hide={hide} index={index}>
    <CardImage hide={hide} src={card.image} alt={card.value} index={index} />
  </CardWrapper>
);

export default Card;
