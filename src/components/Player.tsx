import React, { FunctionComponent } from "react";

import { Card as CardInterface } from "../models/Card";
import { PlayerCorner, CardsErea } from "../styles/PlayerCorner";
import { Score } from "../styles/Score";

import Card from "./Card";

interface Props {
  cards: CardInterface[];
  score: number;
  haveToShow: boolean;
}

const Player: FunctionComponent<Props> = ({
  cards,
  score,
  haveToShow = true,
}) => (
  <PlayerCorner>
    <CardsErea>
      {cards.map((card, index) => {
        const hide = !haveToShow && index === 0;
        return (
          <Card
            key={`card-${card.code}`}
            card={card}
            hide={hide}
            index={index}
          />
        );
      })}
    </CardsErea>
    <Score hide={!haveToShow}>{haveToShow ? score : ""}</Score>
  </PlayerCorner>
);

export default Player;
