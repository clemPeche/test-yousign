import styled from "styled-components";
import { colors } from "../constants/styles";

export const Game = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  flex-grow: 3;
  background-color: ${colors.atronaut};
`;
