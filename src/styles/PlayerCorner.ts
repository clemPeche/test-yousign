import styled from "styled-components";
import { screens } from "../constants/styles";

export const PlayerCorner = styled.div`
  display: flex;
  flex-direction: column;
`;

export const CardsErea = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  height: 8rem;
  margin: 2rem;
  border: solid 5px white;
  border-radius: 25px;

  @media (min-width: ${screens.medium}) {
    height: 16rem;
  }
`;
