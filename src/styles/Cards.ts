import styled from "styled-components";

import { colors, screens } from "../constants/styles";

interface Props {
  hide: boolean;
  index: number;
}

export const CardWrapper = styled.div<Props>`
  max-height: 7rem;
  max-width: 5rem;
  margin: 1rem;
  background-color: ${colors.juniper};
  border: ${({ hide }) => (hide ? `solid 10px ${colors.swirl}` : "none")};
  box-sizing: border-box;
  border-radius: 10px;
  transform: ${({ index }) => `translate(${-120 * index}%)`};

  @media (min-width: ${screens.medium}) {
    max-height: 14rem;
    max-width: 10rem;
    transform: ${({ index }) => `translate(${-80 * index}%)`};
  }
`;

export const CardImage = styled.img<Props>`
  max-height: 7rem;
  max-width: 5rem;
  backface-visibility: hidden;
  transform: ${({ hide }) => (hide ? "rotateX(180deg)" : "rotateX(0deg)")};

  @media (min-width: ${screens.medium}) {
    max-height: 14rem;
    max-width: 10rem;
  }
`;
