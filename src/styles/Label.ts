import styled from "styled-components";
import { colors } from "../constants/styles";

export const Label = styled.span`
  margin: 1rem;
  text-align: center;
  color: ${colors.swirl};
  font-size: 2em;
`;
