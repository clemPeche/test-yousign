import styled from "styled-components";
import { colors } from "../constants/styles";

interface Props {
  hide: boolean;
}

export const ControlerBanner = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 7rem;
  position: absolute;
  bottom: 0;
  width: 100%;
  background-color: ${colors.fountainBlue};
`;

export const ControlerWrapper = styled.div<Props>`
  display: flex;
  justify-content: space-around;
  align-items: center;
  position: absolute;
  bottom: 0.5rem;
  background-color: ${colors.fountainBlue};
  transform: ${({ hide }) => `scaleX(${hide ? "0" : "1"})`};
  transition: all 1s ease-out;
`;
