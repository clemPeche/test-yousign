import styled from "styled-components";
import { colors } from "../constants/styles";

interface Props {
  hide: boolean;
}

export const Score = styled.div<Props>`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 10px;
  width: 5rem;
  height: 5rem;
  background-color: ${colors.atronaut};
  padding: 0.5rem;
  border: solid 5px ${colors.white};
  border-radius: 50%;
  color: ${colors.white};
  font-size: 3em;
  font-weight: bold;
  transform: ${({ hide }) => `scale(${hide ? 0 : 1})`};
  transition: all 0.5s ease-in;
  z-index: 1;
`;
