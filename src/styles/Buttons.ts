import styled from "styled-components";
import { colors } from "../constants/styles";

const Button = styled.button`
  margin: 1rem;
  padding: 1rem;
  font-size: 1em;
  font-weight: bold;
  border-radius: 5px;
  border: none;
`;

export const StarterButton = styled(Button)`
  background-color: ${colors.diserria};
  color: ${colors.atronaut};
`;

export const HitButton = styled(Button)`
  background-color: ${colors.diserria};
  color: ${colors.atronaut};
`;

export const StayButton = styled(Button)`
  background-color: ${colors.swirl};
  color: ${colors.atronaut};
`;
