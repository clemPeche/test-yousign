import { Card } from "../models/Card";
import { cardsValues } from "../constants/cardsValues";

const calculateScore = (cardsScore: number[]) =>
  cardsScore.reduce((acc, curr) => {
    acc += curr;
    return acc;
  }, 0);

export const getScore = (cards: Card[]) => {
  // first we map cards to their values with 1 for aces
  const playerCardsValues = cards.map((card) => cardsValues[card.value]);

  // then we calculate its current score
  const currentScore = calculateScore(playerCardsValues);

  // if our current score allow the user to get an higher range ace value
  // then we granted its value
  if (currentScore <= 11) {
    const indexOfAce = playerCardsValues.indexOf(1);
    if (indexOfAce !== -1) {
      playerCardsValues[indexOfAce] = 11;
    }
  }

  // now we have to return this updated value
  return calculateScore(playerCardsValues);
};
