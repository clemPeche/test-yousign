import axios, { AxiosInstance, AxiosPromise } from "axios";

const baseURL = "https://deckofcardsapi.com/api/deck";

const Api = class {
  public readonly httpClient: AxiosInstance;

  constructor() {
    this.httpClient = axios.create({
      baseURL,
    });
  }

  getNewShuffledDeck = (): AxiosPromise =>
    this.httpClient.get("new/shuffle/", {
      params: {
        deck_count: 1,
      },
    });

  drawCard = (deckId: string, numberOfCardToDraw: number): AxiosPromise =>
    this.httpClient.get(`${deckId}/draw/`, {
      params: {
        count: numberOfCardToDraw,
      },
    });
};

export default new Api();
