import React from "react";

import useGame from "./hooks/useGame";
import Player from "./components/Player";
import Controler from "./components/Controler";

import { AppWrapper } from "./styles/AppWrapper";
import { Game } from "./styles/Game";

const App = () => {
  const {
    isGameFinished,
    bank,
    player,
    isBankPlaying,
    fetching,
    startGame,
    hit,
    stay,
  } = useGame();

  // TODO: Find a way to call useGame from chilren to get rid of all those props
  return (
    <AppWrapper>
      <Game>
        <Player
          cards={bank.cards}
          score={bank.score}
          haveToShow={isBankPlaying}
        />
        <Player cards={player.cards} score={player.score} haveToShow={true} />
      </Game>
      <Controler
        isGameFinished={isGameFinished}
        bankHasWon={bank.hasWon}
        playerHasWon={player.hasWon}
        startGame={fetching ? undefined : startGame}
        hit={fetching ? undefined : hit}
        stay={fetching ? undefined : stay}
      />
    </AppWrapper>
  );
};

export default App;
